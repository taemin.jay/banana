const express = require('express')
const app = express()
const http = require('http')
const cors = require('cors')
const bodyParser = require('body-parser')
const path = require('path')
const fs = require('fs')

const httpServer = http.createServer(app);

app.use(express.static(__dirname + '/build/static/css'));
app.use(express.static(__dirname + '/build/static/js'));
app.use(express.static(__dirname + '/build/'));
app.use(express.static(__dirname + '/build/static/images'));
app.use(express.static(__dirname + '/build/static/videos'));
app.use(express.static(__dirname + '/build/static/fonts'));


app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))


app.get('*', function(req,res) {
    res.sendFile(path.join(__dirname + '/build/index.html'));
})

app.post('/send', function(req,res) {
  console.log(req);
  fs.writeFile('./answers/'+req.body.name+'txt', JSON.stringify(req.body), function (err) {
    if (err) throw err;
    res.send('OK')
  });
})

httpServer.listen(3003);
