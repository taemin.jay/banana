import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import './assets/css/index.css'

import Pages from "./components/Pages/Pages"

import {createStore} from 'redux'
import allReducers from "./reducers"
import {Provider} from 'react-redux'

const store = createStore (allReducers);

class App extends Component {
    render() {
        return (
            <div className="App">
                <div className="contain">
                    <Provider store={store}>
                        <Pages/>
                    </Provider>
                </div>
            </div>
        );
    }
}

export default App;
