import React, {Component} from "react";
import axios from "axios";

class Lesson1 extends Component {
    constructor() {
        super();
        this.state = {
          disabledBtn: true,
        };
        this.form = {
            WEB: null,
          DNS: null,
          TCP: null,
          'client-server': null,
          http: null,
          https: null,
          name: null,
          'Интернет': null
        }
    }

  sendData = () => {
        axios.post('http://localhost:3003/send', this.form)
          .then(res => {
              console.log(res);
          })
  }

  getAnswer = (e,name) => {
      this.form[name] = e.target.value;
    console.log(this.form);
    this.checkAnswers();
  }

  checkAnswers = () => {
        const keys = Object.keys(this.form);
        let notEmpty = 0
        keys.forEach( key => {
            if (this.form[key] === null || this.form[key].length === 0) {
              notEmpty++
            }
        })

    if (notEmpty === 0) {
      this.setState({
        disabledBtn: false
      })
    } else {
      this.setState({
        disabledBtn: true
      })
    }
  }

    render() {
        return (
            <div className="lesson1">
              <h1>Вопросы №1</h1>
                <form>
                  <div className="form-item">
                    <label>Как тебя зовут</label>
                    <input onInput={(e) => this.getAnswer(e,'name')}/>
                  </div>
                  <div className="form-item">
                    <label>Что такое Интернет</label>
                    <textarea onInput={(e) => this.getAnswer(e,'Интернет')}/>
                  </div>
                  <div className="form-item">
                    <label>Что такое WEB</label>
                    <textarea onInput={(e) => this.getAnswer(e,'WEB')}/>
                  </div>
                  <div className="form-item">
                    <label>Объясните своими словами работу клиент-серверного приложения (что происходит, когда ты пытаешься открыть сайт)</label>
                    <textarea onInput={(e) => this.getAnswer(e,'client-server')}/>
                  </div>
                  <div className="form-item">
                    <label>Что такое DNS (своими словами)</label>
                    <textarea onInput={(e) => this.getAnswer(e,'DNS')}/>
                  </div>
                  <div className="form-item">
                    <label>Что такое TCP (своими словами)</label>
                    <textarea onInput={(e) => this.getAnswer(e,'TCP')}/>
                  </div>
                  <div className="form-item">
                    <label>Что такое http</label>
                    <textarea onInput={(e) => this.getAnswer(e,'http')}/>
                  </div>
                  <div className="form-item">
                    <label>Что такое https</label>
                    <textarea onInput={(e) => this.getAnswer(e,'https')}/>
                  </div>
                  <div className="form-item">
                    <label>Какая информация или какие файлы являются основой для сайта? Какие еще бывают файлы?</label>
                    <textarea onInput={(e) => this.getAnswer(e,'files')}/>
                  </div>
                  <button onClick={this.sendData} disabled={this.state.disabledBtn} className={this.state.disabledBtn ? 'disabled' : ''} type="button">Ответить</button>


                </form>
            </div>
        );
    }
}

export default Lesson1;
