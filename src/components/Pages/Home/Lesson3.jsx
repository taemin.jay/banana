import React, {Component} from "react";
import axios from "axios";

class Lesson3 extends Component {
    constructor() {
        super();
        this.state = {
          disabledBtn: true,
        };
        this.form = {
            1: null,
            2: null,
            3: null,
            4: null,
            5: null,
            6: null,
            7: null,
            name: null,
        }
    }

  sendData = () => {
        axios.post('http://localhost:3003/send', this.form)
          .then(res => {
              console.log(res);
          })
  }

  getAnswer = (e,name) => {
        console.log(e.target.value,name);
        this.form[name] = e.target.value;
        console.log(this.form);
        this.checkAnswers();
  }

  checkAnswers = () => {
        const keys = Object.keys(this.form);
        let notEmpty = 0
        keys.forEach( key => {
            if (this.form[key] === null || this.form[key].length === 0) {
              notEmpty++
            }
        })

    if (notEmpty === 0) {
      this.setState({
        disabledBtn: false
      })
    } else {
      this.setState({
        disabledBtn: true
      })
    }
  }

    render() {
        return (
            <div className="lesson3">
              <h1>Вопросы №2</h1>
                <form>
                  <div className="form-item">
                    <label>Как тебя зовут</label>
                    <input onInput={(e) => this.getAnswer(e,'name')}/>
                  </div>
                  <div className="form-item">
                      <label>Объясните своими словами работу клиент-серверного приложения (что происходит, когда ты пытаешься открыть сайт)</label>
                      <textarea onInput={(e) => this.getAnswer(e,'1')}/>
                  </div>
                    <div className="form-item">
                        <label>Что такое DNS (своими словами)</label>
                        <textarea onInput={(e) => this.getAnswer(e,'2')}/>
                    </div>
                    <div className="form-item">
                        <label>Что такое TCP (своими словами)</label>
                        <textarea onInput={(e) => this.getAnswer(e,'3')}/>
                    </div>
                  <div className="form-item questions">
                    <label>Что включает в себя элемент HTML?</label>
                    <div className="radio-answer d-flex flex-row justify-content-between">
                        <div className="d-flex flex-column">
                            <div className="input-radio">
                                <label htmlFor="2-1">Содержимое</label>
                                <input id="2-1" name="2" className="input-check" value="Содержимое" onChange={(e) => this.getAnswer(e,'4')} type="radio"/>
                            </div>
                            <div className="input-radio">
                                <label htmlFor="2-2">Когда один элемент вложен в другой</label>
                                <input id="2-2" name="2" className="input-check" value="Открывающий и закрывающий тег" onChange={(e) => this.getAnswer(e,'4')}  type="radio"/>
                            </div>
                        </div>
                        <div className="d-flex flex-column">
                            <div className="input-radio">
                                <label htmlFor="2-3">Для самозакрывающих элементов</label>
                                <input id="2-3" name="2" className="input-check" value="Открывающий и закрывающий тег" onChange={(e) => this.getAnswer(e,'4')}  type="radio"/>
                            </div>
                            <div className="input-radio">
                                <label htmlFor="2-4">Когда после одного элемента идет такой же</label>
                                <input id="2-4" name="2" className="input-check" value="Открывающий тег" onChange={(e) => this.getAnswer(e,'4')}  type="radio"/>
                            </div>
                        </div>
                    </div>
                  </div>
                    <div className="form-item questions">
                        <label>Напишите в правильном порядке</label>
                        <p>&#60;/head&#62;, &#60;/body&#62;, &#60;html&#62;, &#60;!DOCTYPE html&#62;, &#60;head&#62;, &#60;/html&#62;, &#60;body&#62;, &#60;/div&#62;, &#60;title&#62;Моя веб-страница&#60;/title&#62;, &#60;div&#62;, &#60;h1&#62;Основной текст&#60;/h2&#62;&#60;meta charset="utf-8" /&#62;</p>
                        <textarea onInput={(e) => this.getAnswer(e,'files')}/>
                    </div>
                  <div className="form-item questions">
                      <label>В какую сторону сдвигает элемент положительное значение bottom?</label>
                      <div className="radio-answer d-flex flex-row justify-content-between">
                          <div className="d-flex flex-column">
                              <div className="input-radio">
                                  <label htmlFor="3-1">Влево</label>
                                  <input id="3-1" name="3" className="input-check" value="Влево" onChange={(e) => this.getAnswer(e,'5')} type="radio"/>
                              </div>
                              <div className="input-radio">
                                  <label htmlFor="3-2">Вправо</label>
                                  <input id="3-2" name="3" className="input-check" value="Вправо" onChange={(e) => this.getAnswer(e,'5')} type="radio"/>
                              </div>
                          </div>
                          <div className="d-flex flex-column">
                              <div className="input-radio">
                                  <label htmlFor="3-3">Вверх</label>
                                  <input id="3-3" name="3" className="input-check" value="Вверх" onChange={(e) => this.getAnswer(e,'5')} type="radio"/>
                              </div>
                              <div className="input-radio">
                                  <label htmlFor="3-4">Вниз</label>
                                  <input id="3-4" name="3" className="input-check" value="Вниз" onChange={(e) => this.getAnswer(e,'5')} type="radio"/>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div className="form-item questions">
                      <label>От чего отсчитывается значение top для position: absolute?</label>
                      <div className="radio-answer d-flex flex-row justify-content-between">
                          <div className="d-flex flex-column">
                              <div className="input-radio">
                                  <label htmlFor="4-1">От верхнего края родителя</label>
                                  <input id="4-1" name="4" className="input-check" value="От верхнего края родителя" onChange={(e) => this.getAnswer(e,'6')} type="radio"/>
                              </div>
                              <div className="input-radio">
                                  <label htmlFor="4-2">От верхнего края исходного положения элемента</label>
                                  <input name="4" id="4-2" className="input-check" value="От верхнего края исходного положения элемента" onChange={(e) => this.getAnswer(e,'6')} type="radio"/>
                              </div>
                          </div>
                          <div className="d-flex flex-column">
                              <div className="input-radio">
                                  <label htmlFor="4-3">От верхнего края браузера</label>
                                  <input name="4" id="4-3" className="input-check" value="От верхнего края браузера" onChange={(e) => this.getAnswer(e,'6')} type="radio"/>
                              </div>
                              <div className="input-radio">
                                  <label htmlFor="4-4">От верхнего края body</label>
                                  <input name="4" id="4-4" className="input-check" value="От верхнего края body" onChange={(e) => this.getAnswer(e,'6')} type="radio"/>
                              </div>
                          </div>
                      </div>
                  </div>
                    <div className="form-item questions">
                        <label>Какое значение position устанавливает фиксированное позиционирование?</label>
                        <div className="radio-answer d-flex flex-row justify-content-between">
                            <div className="d-flex flex-column">
                                <div className="input-radio">
                                    <label htmlFor="5-1">static</label>
                                    <input id="5-1" name="5" className="input-check" value="static" onChange={(e) => this.getAnswer(e,'7')} type="radio"/>
                                </div>
                                <div className="input-radio">
                                    <label htmlFor="5-2">fixed</label>
                                    <input name="5" id="5-2" className="input-check" value="fixed" onChange={(e) => this.getAnswer(e,'7')} type="radio"/>
                                </div>
                            </div>
                            <div className="d-flex flex-column">
                                <div className="input-radio">
                                    <label htmlFor="5-3">absolute</label>
                                    <input name="5" id="5-3" className="input-check" value="absolute" onChange={(e) => this.getAnswer(e,'7')} type="radio"/>
                                </div>
                                <div className="input-radio">
                                    <label htmlFor="5-4">relative</label>
                                    <input name="5" id="5-4" className="input-check" value="relative" onChange={(e) => this.getAnswer(e,'7')} type="radio"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button onClick={this.sendData} disabled={this.state.disabledBtn} className={this.state.disabledBtn ? 'disabled' : ''} type="button">Ответить</button>

                </form>
            </div>
        );
    }
}

export default Lesson3;
