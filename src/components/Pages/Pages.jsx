import React from 'react';
import { Switch, Route } from 'react-router-dom'
import Lesson1 from './Home/Lesson1'
import Lesson2 from './Home/Lesson2'
import Lesson3 from './Home/Lesson3'


const Pages = () => (
    <Switch>
        <Route exact path='/lesson1' component={Lesson1}/>
        <Route exact path='/lesson2' component={Lesson2}/>
        <Route exact path='/lesson3' component={Lesson3}/>
    </Switch>
);

export default Pages
