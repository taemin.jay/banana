const initialState = {
    contacts: {
        telephone: "8 800 700 08 08"
    },
    timesheets: "9:00-21:00*, ежедневно",
    currentDate: new Date().getFullYear(),
    city: "Набережные Челны",
    cityShow: false,
    form: false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case "CHANGE_CITY":
            state.city = action.data
            state.cityShow = !state.cityShow
            return {...state}
        case "OPEN_CITIES":
            state.cityShow = !state.cityShow
            return {...state}
        case "CLOSE_FORM":
            state.form = !state.form
            return {...state}
        default:
            return state
    }
}
