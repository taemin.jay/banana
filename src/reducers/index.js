import {combineReducers} from 'redux';
import DengaIvest from './dengainvest';
import Routes from './routes';


const allReducers = combineReducers({
    dengainvest: DengaIvest,
    routes: Routes
});

export default allReducers
