const initialState = {
    disabled: true,
    show: false,
    id: 1,
    name: "",
    desc: "",
    additional: null,
    routes: [
        {
            disabled: false,
            id: 1,
            name: "О компании",
            desc: "О компании",
            link: "/about"
        },
        {
            disabled: true,
            id: 2,
            name: "Документы",
            desc: "Документы",
            link: "/docs"
        },
        {
            disabled: true,
            id: 3,
            name: "Займы",
            desc: "Займы",
            link: "/loan"
        }
    ]
}

export default function (state = initialState, action) {
    switch (action.type) {
        default:
            return state
    }
}
